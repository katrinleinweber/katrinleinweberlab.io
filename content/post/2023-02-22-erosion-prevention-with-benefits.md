---
title: Erosion prevention with benefits
date: 2023-02-22
---


One upon a time, there was a plot of land at the Catalan Mediterranean coast.
It was situated on a North slope approx. 100m above sea level.
Its half-funnel topology and serpentine dirt road lead to rainwater run-off.
Drought and sheep grazing had additionally caused noticeable soil erosion risks.

![](../img/erosion-before.jpg)

_For reference, the wide part of that trench easily held a large adult's foot._

## Three hints

An artificial soil and sand catchment was already being used in a water canal:

![](../img/sand-catchment.jpg)

_Blankets and rocks as an artificial sand catchment._

As a second brain-teaser, dry vine twigs from the property's own vineyard were available.
Normally, they became fire starters for its legendary BBQs.

Lastly, we knew of twig-using silt catchment designs from a different sea.
Around the North Sea coast, [Fascines](https://en.wikipedia.org/wiki/Fascine) or
"[Lahnungen](https://de.wikipedia.org/wiki/Lahnung#/media/Datei:LahnungSuedwesthoern.jpg)"
are used to encourage sedimentation as one step in the process of creating land.

## Retain water, soil and biomass (on the cheap!)

With these hints in mind and the conviction to apply some aspects of
[permaculture theory](https://en.wikipedia.org/wiki/Permaculture#Theory):

- reducing the flow speed of water, so that it can seep into the soil,
- reusing what may at first appear as waste,
- keeping bare soil covered as much as possible

the idea was born to create sand and water catchments,
using the vine twigs as experimental mulches and damns.

![](../img/erosion-prevention.jpg)

_Experimental use of dry twigs bundles to fill up erosion trenches._

We got lucky with a rainy day shortly after these experiments were set up.
The result was an almost complete sequestration of the biomass by sand.

![](../img/erosion-reduced.jpg)

_Outcome of the anti-erosion twig experiment:_
_Water was slowed sufficiently, avoiding further erosion._

## Conclusion

This confirmed the soundness of this general idea.
By rolling out this approach to more locations on the property,
we're hoping to achieve multiple goals:

- sequestering waste biomass like vine cuttings
- letting rainfall help refill existing erosion trenches
- reducing water flow speed and soil loss
- reducing the necessity for later, more expensive road maintenance

## Another example

Coincidentally, the properties football field was being cleaned off herbs at the same time.
Again, the pieces of turf seemed like waste product initially, but could perfectly be used
as patches to cover wider erosion channels, thus creating something like
[Swales](https://en.wikipedia.org/wiki/Swale_(landform)).

![](../img/erosion-large.jpg)

![](../img/erosion-patches.jpg)

Working from the bottom of the property upwards,
we observed were water run-off originated from,
and "patched" those locations with turf from the field.

In locations with deeper erosion channels, 2 layers were used,
putting one turf upside down at the bottom and another right-side-up on top.

As with the twig bundles, this ensures that some amount of biomass will be sequestered.
This process occurs essentially by itself, whenever a rainfall event
is strong enough to cause some level of soil erosion from locations
that have not been patches, yet.
