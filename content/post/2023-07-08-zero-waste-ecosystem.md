---
title:  Ein unverpacktes, minimalistisches und billiges ZeroWaste-Ökosystem für deinen Haushalt
date: 2023-07-08
tags:
  - DIY
  - ZeroWaste
---

## Das Problem

Für regelmäßiges Einkaufen in Unverpackt-Läden benötigen wir viele Vorratsgefäße.
Oft sind diese auch aus Plastik, was ein wenig der ZeroWaste-Idee entgegensteht,
auch wenn haltbare Plastikgefäße natürlich langfristig wenig problematisch sind.
Kurzfristig können allerdings die Anschaffungskosten beachtenswert sein,
insb. wenn wir ein zusammenpassendes System eines bewährten Herstellers nutzen.

Wenn wir dagegen alle möglichen Gefäße nutzen,
die sich im Laufe der Jahre im Haushalt angesammelt haben,
muss ich gestehen, dass mich die resultierende Unordnung nervt:
Je mehr verschiedenen Plastik- und Glasgefäße,
desto schwieriger werden Transport, Befüllung, Säuberung, und Aufbewahrung.
Kurzum: Regale, die Tetris-Spielfeldern gleichen, bringen Unruhe in die Küche.

## Die Lösung(sideen)

### Passata- und/oder Joghurt-Gläser als Standardgefäße nutzen

Hier meine ich konkret die:

- hohen, ca. 750mL fassenden Passatagläser (meist ohne Pfand)
- die bauchigen, ca. 500mL fassenden Joghurtpfandgläser

Da ihre Deckel ebenfalls Spülmaschinen-fest sind,
eignen sich diese beiden Gefäßtypen m.E. besonders gut
für die langfristige Nutzung als Vorratsgefäße.
Beide können problemlos weiter verwendet werden,
fassen für 2- bis 3-Personen-Haushalte passende Portionen an
Reis, Hülsenfrüchten, Müsli, Sojagranulat, etc.,
und die mir bekannten Unverpackt-Läden, haben zudem Spender und Trichter,
die genau auf die Hälse von Passata- und Joghurt-Gläsern passen.

Einzige Wermutstropfen:

-
- Speziell bei Pfandgläsern gäbe es noch den Aspekt ihres Umlaufs zu beachten:
  Sie nutzende Firmen haben ein finanzielles Interesse an der schnellen Rückgabe,
  was der hier präsentierten Idee entgegensteht.
  Sollten signifikante Mengen von z.B. 500mL-Joghurtgläsern Monate- bis Jahre-lang
  in Haushalten rumstehen, müsste dies bei der Nachproduktion berücksichtigt werden.

Unterm Strich halte habe ich jedoch die Standardisierung von Vorratsgefäßen
auf hauptsächlich diese Typen für sinnvoll und habe dies in meinem Haushalt
in den letzten Jahren vorangetrieben.

Bleibt ein problematischer Aspekt: Der Transport solcher Gefäße beim Einkauf.

### Tragetaschen für diese Gläser

Für Bier- und Wein-Flaschen gibt es schon
[solche Taschen](https://www.etsy.com/listing/632717879).
Auch [Schnittmusterbeispiele](https://duckduckgo.com/?q=flaschentasche+schnittmuster)
für die Hobbynäherei sind vorhanden.
Aber für die Formfaktoren "Passata" oder "Joghurt" habe ich bisher keine gefunden.

Aufgrund der großen Überschneidung der Marktsegmente "Upcycling" und "ZeroWaste".
Insb. zum Upcycling alten, festen Jeansstoffes würde sich diese Produktkategorie m.E. eignen.
Das Innenfutter dagegen soll nur das Zusammenstoßen der Gläser verhindern,
kann also auch aus leichteren Stoffresten oder Altkleidern sein, notfalls doppelt gelegt.

### Regaleinsätze und Spender für o.g. Standardgläser

Aus ge-laser-tem Holz zum Zusammenstecken, oder aus 3D-gedrucktem Plastik
gibt es ebenfalls schon einige
[Beispiele](https://thangs.com/search/stackable%20AND%20%28can%20OR%20jar%29%20AND%20dispenser?scope=all&view=grid)
für andere
[Gefäßtypen](https://cults3d.com/en/3d-model/home/soda-machine-for-fridge-or-other).

Laser-cuts für gängige (Pfand)Gläser gibt es schon :-)

- [500mL Joghurt](https://boxes.hackerspace-bamberg.de/WineRack?FingerJoint_style=rectangular&FingerJoint_surroundingspaces=2.0&FingerJoint_bottom_lip=0.0&FingerJoint_edge_width=1.0&FingerJoint_extra_length=0.0&FingerJoint_finger=2.0&FingerJoint_play=0.0&FingerJoint_space=2.0&FingerJoint_width=1.0&x=590&y=290&h=120&radius=45.0&walls=minimal&thickness=3.0&format=svg&tabs=0.0&qr_code=0&debug=0&labels=0&labels=1&reference=100.0&inner_corners=loop&burn=0.1&language=en&render=0)
- [750mL Passata](https://boxes.hackerspace-bamberg.de/WineRack?FingerJoint_style=rectangular&FingerJoint_surroundingspaces=2.0&FingerJoint_bottom_lip=0.0&FingerJoint_edge_width=1.0&FingerJoint_extra_length=0.0&FingerJoint_finger=2.0&FingerJoint_play=0.0&FingerJoint_space=2.0&FingerJoint_width=1.0&x=590&y=290&h=160&radius=40.0&walls=minimal&thickness=3.0&format=svg&tabs=0.0&qr_code=0&debug=0&labels=0&labels=1&reference=100.0&inner_corners=loop&burn=0.1&language=en&render=0)

S-förmige Spender machen es für lange haltbare Waren
(eben Trockenes aus dem Unverpackt-Laden)
sehr bequem, den jeweils:

- ältesten Vorrat zur Verwendung aus dem Regal rauszugreifen,
- neuesten Einkauf oben auf den Vorratsstapel zu legen.

Etwas Sorge könnte mensch hier haben, dass aufeinanderliegende Gläser
sich gegenseitig zerkratzen, oder beim Durchrollen und Aufeinanderprallen splittern.
Mittels zweier Banderolen aus selbst-klebendem Filz- oder Gummi-Band
(z.B. Baumarktware für Teppiche, Möbel oder Fensterdichtungen)
sollte sich dieses Risiko jedoch vermeiden lassen.
Evtl. wären die Gläser dann nicht mehr Spülmaschinen-fest,
aber solange nur trockene Nahrungsmittel eingefüllt werden,
dürfte regelmäßiges Ausspülen mit heißem Wasser ohnehin ausreichen.

Bitte helft mit, diese Idee an Werkhaus und Unverpacktläden heranzutragen,
sowie an Leute in der Szene, die schon Etsy-Shops haben.
