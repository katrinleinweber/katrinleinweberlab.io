---
title: "CoVID? Nein Danke!"
date: 2023-12-06
subtitle: "Persönliche Schutzmaßnahmen für das 21. Jahrhundert"
tags:
  - CoVID
---

Um den immer wieder mal privat verteilten Hinweisen ein zentrales Zuhause zu geben,
hier die CoVID-Vermeidungstrategien meines Haushalts.
Keine davon ist perfekt, aber jede davon ist besser als keine ([Schweizer-Käse-Modell][SKM]).

1. [FFP2]/[3][FFP3]-Masken in der Nähe von Menschen aus anderen Haushalten tragen,
   sowie in allen Innenräumen, in denen nicht mess- oder sichtbar die Luft gereinigt
   oder mit Frischluft verdünnt wird (siehe 3. & 4.).
   - Auch [Made in Germany][mig] & [Made in Europe][mie] gibt es.
   - Wer Bock auf Masken als buntes Fashion-Accessoire hat:
     [MaskLab.global](https://store.masklab.global/collections/kf-series)
   - Sammelbestellung im Nachbarschafts-, Familien- & Freundeskreis organisieren!
     Das wird alles noch ein [paar Jährchen][pmc] dauern,
     und Masken helfen ja auch gegen Luftverschmutzung, echte Erkältungen, usw. 🤷‍♀️
2. Vor und nach einem Risikokontakt [CPC]-haltiges Mundwasser nutzen.
   "Cetyl-Pyridinium-Chlorid" ist nicht in _allen_ Mundwässern im Drogeriemarkt drin,
   aber z.B. in [dm Dontodent antibakteriell][dmd].
   - "Risikokontakt" hier heißt wie in 1.: "[Innenräume mit] Menschen aus anderen Haushalten".
   - Für längere CPC-Versorgung (Bus, Bahn, Veranstaltungen, etc.)
     gibt es [Lutschtabletten][tab].
3. HEPA+UV-basierte [Luftreiniger][iaq] zuhause nutzen,
   insb. wenn Besuch da ist.
   Beim Essen mit Gästen wird der Luftauslass so positioniert,
   dass die gereinigte Luft möglichst quer über den Tisch pustet.
   - Baulich ideal wären Kombigeräte aus normaler Deckenleuchte und
     nach oben gerichteten UV-Strahlern ([UVGI]; idealerweise plus Ventilator).
4. [PlusLife][plli]-Testgerät nutzen, statt Antigen-Schnelltests.
   Es ist beinahe so sensitiv wie PCR und Proben von 2, 3 oder 4 Personen können gemischt werden.
   Letztendlich ist egal, wer im Haushalt positiv ist, denn bei dem Ergebnis sollten sich alle erstmal schützen,
   und getrennte Folgetests in den nächsten Tagen klären die Situation dann schon noch auf.
5. CO2-Sensor (z.B. [Aranet][ara]) im Blick haben und durchlüften,
   Lüftungsempfehlungen gegen Schimmel laufen im Herbst/Winter auf dasselbe hinaus:
   _mehrmals täglich_, sobald es sich dem [gelben Bereich][co2] (800ppm) nähert.
   Bei >15ºC Außentemperatur: einfach gegenüberliegende Fenster gekippt lassen.
   - Baulich ideal wäre Frischluftzufuhr über einen Wärmetauscher.
   - Ab 800ppm CO2 kann von [stundenlangem][celeb] Rumwabern der Viren ausgegangen werden.
     So, wie Zigarettenrauch ja auch lange im Raum hängt.
     Daher: FFP2, auch wo Jemandes vorherige Ausatemluft noch vorhanden sein könnte.

[celeb]: https://www.youtube.com/watch?v=Wd600oauf_I
[ara]: https://shop.aranet.com/europe/product/aranet4-home
[co2]: https://shop.aranet.com/europe/product/Rebreathed-Sticker-Collection
[CAS]: https://cleanairstars.com/
[CPC]: https://de.wikipedia.org/w/index.php?title=Cetylpyridiniumchlorid&oldid=239925997
[dmd]: https://www.dm.de/dontodent-mundspuelung-antibakterielle-mundhygiene-p4066447190298.html
[FFP2]: https://www.shop-sks.com/3M-9320-Atemschutzmaske-Aura-FFP2-bis-zum-10-fachen-des-Grenzwertes-einzelverpackt-Bulkware-440-Stueck-im-Karton
[FFP3]: https://seyffer.shop/de/3m-aura-atemschutzmaske-9330-ffp3-nr-d-industrievariante-hygienisch-einzelverpackt-pandemiemaske.html
[iaq]: https://filters.cleanairstars.com/?country=US&max-an=50&ach=6&room-size=40&m3-or-cu=m3&no-of-occ=&max_units=10&schedule=no&diy=no&tariff=0.22&filter-rs=12&lifetime=4&submit=submit
[mie]: https://duckduckgo.com/?q=(FFP2+OR+FFP3)+AND+%22Made+in+Europe%22&t=ffab&ia=web
[mig]: https://duckduckgo.com/?t=ffab&q=(FFP2+OR+FFP3)+AND+%22Made+in+Germany%22&ia=web
[pmc]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8223470/#sec2.3title
[SKM]: https://virologydownunder.com/the-swiss-cheese-infographic-that-went-viral/
[tab]: https://de.wikipedia.org/wiki/Cetylpyridiniumchlorid#Handelsnamen
[UVGI]: https://duckduckgo.com/?q=upper+room+germicidal+uv+light&t=ffab&ia=web
[plli]: https://virus.sucks/pluslife/

### Informationsquellen zu aktuellen CoVID-Situation

- [SARS-CoV-2-Konzentration im Abwasser](https://edoc.rki.de/handle/176904/11665)
  (wöchentl. AMELAG-Bericht für viele deutsche Städte)
- inoffizielle Visualisierung:
  [@RV_Enigma](https://usky.app/profile/rv-enigma.bsky.social).
  [RIP Roland Jäger](https://archive.is/VfsGh) 😔
- Überblicke über aktuelle Studien:
  [@RWittenbrink](https://farside.link/nitter/RWittenbrink) &
  [CoVIDisNotOver.info](https://covidisnotover.info/)
- Journalistische Angebote:
  [Mainz&](https://mainzund.de/hohe-krankenstaende-folgen-von-corona-dramatisch-unterschaetzt-mainz-neue-coronawelle-rollt-aktuell/),
  [Mittelländische Zeitung](https://www.dmz-news.eu/gesundheit-wissen-1/),
  die [taz zu LongCoVID](https://taz.de/Long-Covid/!t5741122/),
  [Britta Domke](https://www.manager-magazin.de/hbm/strategie/long-covid-und-me-cfs-am-arbeitsplatz-ups-jetzt-habe-ich-das-boese-wort-gesagt-a-7a4b5c23-aebc-4a24-888f-c4274d9a1e89)
  im [Manager-Magazin](https://web.archive.org/web/20231213105635/https://www.manager-magazin.de/harvard/long-covid-langzeitfolgen-schwaechen-den-arbeitsmarkt-a-26e2413c-076c-43ac-b3ef-5bef4348a4ac) (!)
- [Endemie-Rebellen.Podigee.io](https://endemie-rebellen.podigee.io/archive)
- [CoronaWissen.com](https://coronawissen.com/)

Ja, die muten zeitweise wie eine Echokammer an,
aber vom Echo der Vorsicht wird mensch eben nicht krank.
Vom Echo der Verharmlosung in vielen (auch öffentlich-rechtlichen)
Medien dagegen schon, siehe [Tagesschau.de/thema/krankenstand](https://www.tagesschau.de/thema/krankenstand).

### Initiativen

- [Bildung Aber Sicher](https://bildungabersicher.net/)
- [Initiative Gesundes Österreich](https://www.igoe.at/)

### Hintergrund, historischer Kontext & Philosophie

- [Wie hat sich die Hygiene in 120 Jahren verändert?](https://archive.is/htutG)
- [Privatisierung des Schutzes, Sozialisierung der Gefahr](https://jungle.world/artikel/2023/47/covid-corona-privatisierung-des-schutzes-sozialisierung-der-gefahr)
- [Was Unternehmen gegen Long Covid tun können](http://web.archive.org/web/20231213105635/https://www.manager-magazin.de/harvard/long-covid-langzeitfolgen-schwaechen-den-arbeitsmarkt-a-26e2413c-076c-43ac-b3ef-5bef4348a4ac)
- [Denkschrift zur Gründung eines Bundesinstitutes für öffentliche Gesundheit](https://www.krankenhaushygiene.de/informationen/informationsarchiv/925)
