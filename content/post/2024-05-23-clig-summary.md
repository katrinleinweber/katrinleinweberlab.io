---
title: "A Summary of Command Line Interface Guidelines"
date: 2024-05-23
tags:
  - shell scripting
---

CLIs are stable and versatile, thus enabling creativity.

> Your software _will_ become a part in a larger system.
> Your only choice is over whether it will be a well-behaved part.  
> -- [CLIg.dev](https://clig.dev/)

##### Philosophy

1. Design for humans, because usage by other programs is less common nowadays.
2. Keep following UNIX conventions: stdin/out, exit codes, plain lines for text output, JSON when structure is needed.
3. Consistency with other tools, because efficiency comes from transferrable experience.
4. _Just enough_ output to avoid impatience.
5. Help users discover help texts, examples, etc.
6. CLI usage often resembles a conversation. The tool should therefore suggest corrections with errors, clarify progress in multi-step processes, etc.
7. Robustness by graceful failure, idempotency, explaining problems, rather than barfing stacktraces, and generally: simplicity.
8. Being a kind, emphatic helper.
9. Abandon standards when harm is demonstrated.

##### Guidelines

1. Use an [argument parser](https://clig.dev/#the-basics).
   Return numeric exit codes, with 0 for success.
   Send output to `stdout` to support `|`ing.
   Send log and error messages to `stderr` to _not_ pipe those.
2. **Help** texts for: no option, `-h` or `--help`.
   Help links back to project repo or website.
   Put example into help texts.
   Structure help texts into a logical order and with formatting.
   Suggest corrections in error messages.
   Don't wait silently for `sdtin` (like `cat` does).
3. **Document** what a tool is _for_ and _not for_ separately from help texts,
   in terminal and on the web.
4. **Output:** Prioritise its human- over machine-readability,
   if necessary with `--plain`/`--json` flags.
   Report success only briefly and offer `-q`uiet option.
   Report state changes to the user
   and offer a `status` subcommand
   that suggests next steps.
   Use ASCII art, [color](https://no-color.org/), emojis, etc. where appropriate.
   Show log levels or debug info only in a `-v`erbose mode.
   Use `less -FIRX` or other pager for outputting more than 1 page of text.
5. **Errors** should be caught internally and rewritten for humans.
   Increase signal-to-noise ratio with grouping,
   and putting most relevant guidance to the end.
   Support bug reporting by writing stacktraces to logfiles,
   and link to a form/template.
6. **Args & flags:** Prefer (named) flags to (positional) arguments.
   Use 1-letter flags sparingly and stick to existing terms.
7. **Subcommands:** help reduce complexity,
   when they enable discovery of related tools
   and use consistent nounds, verbs, flags, etc.
8. **Robustly** handle user input: early, with understandable errors.
   Respond within 100ms and before longer operations.
   Network operations should have timeouts.
   Parallelism is useful, but makes output 10x harder.
9. **Interface changes** should be additive,
   rather than breaking existing behaviour.
   Follow [SemVer](https://semver.org/) conventions
   to announce unavoidable breaking changes.
   Don't allow abbreviations or catch-all in the first place.
10. **Respect signals** like `Ctrl+c` and inform user
   about ongoing cleanup operations.
11. **Configuration** options should be informed by
   specificity, stability and complexity.
   Varying for each invocation: flags.
   Stable within a project but not for each user: flags & env vars.
   Stable for all users within a project: config file,
   according to [XDG](https://freedesktop.org/).
   Never append to system-wide config file,
   unless you also date & comment your addition.
   Use this order also for precedence: flags to sys-wide.