#!/bin/bash

file="$1"
slug="$2"

# https://github.com/ggerganov/whisper.cpp/
tool="$HOME/GitHub.com/whisper.cpp"
size="${3:-medium}"

# Prepare Hugo-compatible blog post file
date=$(date -u +%Y-%m-%d)
blog="$date-$slug"

# Convert input to Whisper's required 16kHz
temp="$slug.wav"
ffmpeg -y \
  -v error \
  -i "$file" \
  -ar 16000 -ac 1 -c:a pcm_s16le \
  "$temp"

# Transcribe to text & show confidence-colored preview
"$tool/main" \
  --model "$tool/models/ggml-$size.bin" \
  --threads 8 \
  --output-txt \
  --print-colors \
  --language auto \
  "$temp" |
  cut -d']' -f2
# Cut whisper's timestamps off the preview

# Create blog post file
title=$(head -1 "$temp".txt)
cat >"$blog".md <<HEADER
---
title: $title
date: $date
---

$(cat "$temp".txt)
HEADER

# Cleanup files & text-prefixed whitespace
trash "$temp"* "$file"
sd '^ ' '' "$blog".md
