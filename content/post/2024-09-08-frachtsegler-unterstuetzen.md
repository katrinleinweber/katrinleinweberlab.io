---
title: "Frachtsegler unterstützen"
date: 2024-09-08
subtitle: "Werbung für eine gute Sache"
tags:
  - essen
  - segeln
---

## Liste von Frachtsegel-Shops

- [Timbercoast][tico]/Avontuur: Das erste derartige StartUp, von dem ich gehört habe.
  Sie verkaufen Kaffee, Schokolade und Rum und haben ihren Heimathafen in Bremen.
- [TOWT][towt] betreibt mehrere moderne Frachtsegler, aus modernen Baustoffe wie Kohlefaser.
  Sie verkaufen ebenfalls Kaffee, Schoki, Rum, aber auch Olivenöl und Bier.
  Zudem bemühen sie sich um die Ausbildungsmodernisierung für Seeleute.
- [Fairtransport][treh] betreibt ebenso wie Timbercoast ein restauriertes Holzschiff.
  Neben ähnlichen Produkten gibt es hier auch Kleidung und Gebrauchsgegenstände.
- [SailCargo/AstilleroVerde Shipyard][saca] scheinen v.A. ein CrowdFunding-Projekt zu sein,
   das Frachtschiffe aus Holz neu baut.
   Der Vollständigkeit halber liste ich es hier mal auf.

## Segelurlaub gefällig?

Die meisten der o.g. Firmen bieten auch bezahlte Mitfahrten an.
Bei "Kost und Logie" sind die Preise zwar noch nicht angekommen,
aber wer schon immer mal ohne CO2-Emissions eine Kreuzfahrt machen wollte
(sowie dabei an Bord mithelfen und ins Segelhandwerk reinschnuppern),
hat hier einige Möglichkeiten.

## Ist das nicht alles quasi Drogentransport?

Tja, naja, irgendwie schon: Alkohol, Koffein, etc.
Offenbar herrscht eine höhere Zahlungswilligkeit für solche _dichten_, hochwertigen Produkte.
[Logistics is hard][mccc].
Somit sind sie eher zur Finanzierung der guten Sache "Frachtsegeln" geeignet,
als … _überleg_ … Kokosmilch, exotische Nüsse, oder so.

Letztere wären auch mir lieber, aber der Weg dorthin benötigt sicherlich Unterstützung,
z.B. in Form des Kaufs aktuell verfügbarer Produkte.

[mccc]: https://media.ccc.de/search/?q=Logistik
[tico]: https://timbercoast.de/collections/frontpage/
[towt]: https://boutique-towt.eu/en/
[treh]: https://www.treshombres-shop.com
[saca]: https://www.sailcargo.inc
